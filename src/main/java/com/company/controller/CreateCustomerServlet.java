package com.company.controller;

import com.company.dao.UserDao;
import com.company.dao.UserDaoImpl;
import com.company.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by heinr on 16.01.2017.
 */
public class CreateCustomerServlet extends HttpServlet {

    private UserDao userDao = new UserDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        // Обрабатываем только запросы на http://localhost:8080/customers
        if (!req.getRequestURI().equals("/createCustomer")) {
            // Если запрос не совпадает с ожидаемым, возвращаем ошибку 404 - страница не найдена
            resp.sendError(404);
            return;
        }

        // Генерируем ответ и записываем его в response
        // Подробнее см. IndexServlet
        resp.setCharacterEncoding("utf-8");

        req.getRequestDispatcher("/WEB-INF/jsp/createNewCustomer.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        // Получаем список параметров из формы
        Map<String, String[]> params = req.getParameterMap();

        // Проверяем какая команда пришла - create или delete
        String uri = req.getRequestURI();
        String command = uri.substring(uri.lastIndexOf("/") + 1);

        // Обрабатываем команду
        switch (command) {
            case "create":

                User user = new User(params.get("userName")[0], params.get("password")[0]);

                userDao.save(user);
                break;

        }

        resp.sendRedirect("/login");
    }
}