package com.company.controller;

import com.company.dao.UserDaoImpl;
import com.company.dao.UserDao;
import com.company.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by heinr on 16.01.2017.
 */
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");

        req.getRequestDispatcher("/WEB-INF/jsp/loginPage.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Получаем параметры из формы
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        UserDao userDao = new UserDaoImpl();
        User user = null;
        try {

            //Получаю пользователя (и пароль)
            user = userDao.get(login);

            if (user != null && user.getUserName().equals(login) && user.getPassword().equals(password)) {

                // Получем сессию.
                // Если параметр true, то сессия будет создана, если её нет.
                // Если параметр false, и сессии нет, то метод вернет null.
                // Если сессия уже есть, метод getSession вернет ее независимо от параметра.
                HttpSession session = req.getSession(true);

                // Добавление атрибута к сессии
                session.setAttribute("login", user.getUserName());
                session.setAttribute("username", user.getUserName());

                // Удаление атрибута
                // session.removeAttribute("blablabla");

                // Успешно авторизовались, перенаправляем на страницу кастомеров
                resp.sendRedirect("/index");
            } else {
                // Авторизация не удалась. Возвращаем на страницу логина
                resp.sendRedirect("/login");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Авторизация не удалась. Возвращаем на страницу логина
            resp.sendRedirect("/login");
        }
    }
}