package com.company.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by heinr on 16.01.2017.
 */
public class IndexPageServlet extends HttpServlet {

    // doGet обрабатывает GET запросы на http://localhost:8080/
    // см. файл web.xml
    // req - объект-запрос браузера
    // resp - объект ответ сервера
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        resp.setCharacterEncoding("utf-8");

        req.getRequestDispatcher("/WEB-INF/jsp/homePage.jsp").forward(req, resp);
    }
}