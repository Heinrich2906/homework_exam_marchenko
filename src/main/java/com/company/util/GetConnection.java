package com.company.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by heinr on 16.01.2017.
 */
public class GetConnection {

    public static Connection getConnection() throws Exception {

        Connection connection = null;

        Properties info = new Properties();
        info.put("user", "postgres");
        info.put("password", "admin");
        connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/blog", info);

        return connection;

    }
}
