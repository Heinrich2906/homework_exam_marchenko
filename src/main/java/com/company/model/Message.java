package com.company.model;

/**
 * Created by heinr on 16.01.2017.
 */
public class Message {

    private String userName;
    private String thema;
    private String note;
    private Long id;

    public Message(String userName, String thema, String note, Long id) {
        this.userName = userName;
        this.thema = thema;
        this.note = note;
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getThema() {
        return thema;
    }

    public void setThema(String thema) {
        this.thema = thema;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;

        Message message = (Message) o;

        if (!getUserName().equals(message.getUserName())) return false;
        if (!getThema().equals(message.getThema())) return false;
        if (!getNote().equals(message.getNote())) return false;
        return getId().equals(message.getId());
    }

    @Override
    public int hashCode() {
        int result = getUserName().hashCode();
        result = 31 * result + getThema().hashCode();
        result = 31 * result + getNote().hashCode();
        result = 31 * result + getId().hashCode();
        return result;
    }
}
