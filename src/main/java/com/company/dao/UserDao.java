package com.company.dao;

import com.company.model.User;

/**
 * Created by heinr on 16.01.2017.
 */
public interface UserDao {

    User get(String userName) throws Exception;
    void save(User user);

}
