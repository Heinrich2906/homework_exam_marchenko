package com.company.dao;

import com.company.util.GetConnection;
import com.company.model.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by heinr on 16.01.2017.
 */
public class UserDaoImpl implements UserDao {

    public User get(String userName) {

        Connection connection = null;
        User user = null;

        try {
            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE userName = ?");

            //2.1. Передача параметра
            statement.setString(1, userName);

            //3. Выполнение запроса
            ResultSet resultSet = statement.executeQuery();

            //4. Обработка результата запроса
            while (!resultSet.isLast()) {
                resultSet.next();

                user = new User(resultSet.getString("userName"),
                        resultSet.getString("password"));
            }

            // Шаг 5.1. Закрыть ResultSet
            resultSet.close();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public void save(User user) {

        Connection connection = null;

        try {

            //1. Зарегистрировать драйвер БД в менеджере
            connection = GetConnection.getConnection();

            //2. Создание запроса
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users (userName, password) VALUES(?,?)");

            //2.1. Передача параметра
            statement.setString(1, user.getUserName());
            statement.setString(2, user.getPassword());

            int blank = statement.executeUpdate();

            // Шаг 5.2. Закрыть Statement
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Шаг 5.3. Закрыть соединение
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
