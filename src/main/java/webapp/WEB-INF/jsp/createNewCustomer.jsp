<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.company.model.Customer" %>
<%@ page import="java.util.List" %>

<html>

    <head>

        <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="title.jsp"/>

    </head>

    <body class="container">

         <%-- Включаем содержимое header.jsp в текущую позицию на странице --%>
        <jsp:include page="header.jsp"/>

        <div class="header">
            Neukunde
        </div>

        <div id="form-container">

            <form action="createCustomer/create" method="post" id="customer-form">

            <label for="fname">Name</label>
            <input class="input-box" type="text" name="userName" placeholder="User Name">

            <label for="lname">Passwort</label>
            <input class="input-box" type="password" name="password" placeholder="Password">

            <button type='submit' class="btn">Erstellen</a>

        </div>

        <script src="/javascript/script.js"></script>

    </body>

</html>

